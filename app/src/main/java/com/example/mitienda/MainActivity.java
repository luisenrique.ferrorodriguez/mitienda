package com.example.mitienda;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ImageButton pboton1=(ImageButton) findViewById(R.id.imageButton5);

        pboton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast notificacion = Toast.makeText(MainActivity.this, "Anillos de Plata", Toast.LENGTH_SHORT);
                notificacion.show();
            }
        });

        }


    public void form(android.view.View view) {

        Intent cambiar = new Intent(MainActivity.this,formulario.class);
        startActivity(cambiar);


    }

    public void isesion(View view) {

        Intent cambiar = new Intent(MainActivity.this,iniciosesion.class);
        startActivity(cambiar);
    }

    public void products(android.view.View view) {

        Intent cambiar = new Intent(MainActivity.this,Productos.class);
        startActivity(cambiar);

    }

    public void fproducts(android.view.View view) {

        Intent cambiar = new Intent(MainActivity.this,MainFragment.class);
        startActivity(cambiar);

    }

    public void salirApp(View view){

        finish();
    }

    @Override public boolean onCreateOptionsMenu(Menu menulateral){

        getMenuInflater().inflate(R.menu.menulateral, menulateral);

        return (true);
    }

    @Override public boolean onOptionsItemSelected(MenuItem menulateral){

        int id=menulateral.getItemId();

        if (id==R.id.registrarse){

            form(null);
        }

        if (id==R.id.iniciarsesion){

            isesion(null);

        }

        if (id==R.id.salir){

            salirApp(null);

        }

        return super.onOptionsItemSelected(menulateral);

    }



    @Override
    public void onClick(View view) {


    }
}