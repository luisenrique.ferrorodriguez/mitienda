package com.example.mitienda;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.mitienda.persistencia.DbHelper;
import com.google.android.material.button.MaterialButton;

public class formulario extends AppCompatActivity {

    Button btnRegistro;

    private Dialog dialog;
    private Button ShowDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        //Dialog
        ShowDialog = findViewById(R.id.btnRegistro); //ID del boton que esta relacionado con el mensaje de dialogo

        Button btnRegistro = (Button) findViewById(R.id.btnRegistro);

        dialog = new Dialog(formulario.this);

        // conectamos la clase .java
        dialog = new Dialog(formulario.this); // Indicamos donde se mostrará el el Dialog
        // conectamos al .xml
        dialog.setContentView(R.layout.custom_dialog); // Conexión al archivo custom_dialog.xml
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.background2));

        }

        dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.background2));// Asiganamos el Fondo del Dialog (Background)
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false); //se retorna a la vista principal (origen)

        // Declaramos los botones
        Button Okey = dialog.findViewById(R.id.btn_okay);
        Button Cancel = dialog.findViewById(R.id.btn_cancel);

        Okey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(formulario.this, "Okay", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(formulario.this, "Cancel", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        ShowDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });


       /* btnRegistro = findViewById(R.id.btnRegistro);
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DbHelper dbHelper = new DbHelper(formulario.this);
                SQLiteDatabase db = dbHelper.getWritableDatabase(); //Crearla = Escribir
                // Validación
                if(db != null){
                    Toast.makeText(formulario.this, "BASE DE DATOS CREADA",Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(formulario.this,"ERROR AL CREAR BASE DE DATOS", Toast.LENGTH_LONG).show();
                }

            }
        });*/


    }
}