-- MySQL Script generated by MySQL Workbench
-- Sat Oct 30 13:26:01 2021
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema dbtienda
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `dbtienda` ;

-- -----------------------------------------------------
-- Schema dbtienda
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dbtienda` DEFAULT CHARACTER SET utf8 ;
USE `dbtienda` ;

-- -----------------------------------------------------
-- Table `dbtienda`.`compras`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbtienda`.`compras` ;

CREATE TABLE IF NOT EXISTS `dbtienda`.`compras` (
  `idcompras` INT NOT NULL AUTO_INCREMENT,
  `preciocompra` INT NOT NULL,
  `iva` FLOAT NOT NULL,
  `totalivainc` FLOAT NOT NULL,
  `fechacompra` DATE NOT NULL,
  `productos_idproductos` INT NULL DEFAULT NULL,
  `tenderos_idtendero` INT NULL DEFAULT NULL,
  PRIMARY KEY (`idcompras`),
  INDEX `fk_compras_productos1_idx` (`productos_idproductos` ASC) VISIBLE,
  INDEX `fk_compras_tenderos1_idx` (`tenderos_idtendero` ASC) VISIBLE,
  CONSTRAINT `fk_compras_productos1`
    FOREIGN KEY (`productos_idproductos`)
    REFERENCES `dbtienda`.`productos` (`idproductos`),
  CONSTRAINT `fk_compras_tenderos1`
    FOREIGN KEY (`tenderos_idtendero`)
    REFERENCES `dbtienda`.`usuarios` (`idusuario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `dbtienda`.`detventas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbtienda`.`detventas` ;

CREATE TABLE IF NOT EXISTS `dbtienda`.`detventas` (
  `consventas` INT NOT NULL,
  `cantidad` INT NOT NULL,
  `precioUnit` FLOAT NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `regventa` INT NOT NULL,
  `ventas_idventas` INT NULL DEFAULT NULL,
  `productos_idproductos` INT NULL DEFAULT NULL,
  PRIMARY KEY (`consventas`),
  INDEX `fk_detventas_ventas1_idx` (`ventas_idventas` ASC) VISIBLE,
  INDEX `fk_productos_idproductos2_idx` (`productos_idproductos` ASC) VISIBLE,
  CONSTRAINT `fk_detventas_ventas1`
    FOREIGN KEY (`ventas_idventas`)
    REFERENCES `dbtienda`.`ventas` (`idventas`),
  CONSTRAINT `fk_productos_idproductos2`
    FOREIGN KEY (`productos_idproductos`)
    REFERENCES `dbtienda`.`productos` (`idproductos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `dbtienda`.`productos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbtienda`.`productos` ;

CREATE TABLE IF NOT EXISTS `dbtienda`.`productos` (
  `idproductos` INT NOT NULL AUTO_INCREMENT,
  `codigoproducto` INT NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `categoria` VARCHAR(45) NOT NULL,
  `cantidad` INT NULL DEFAULT NULL,
  `precioUnit` FLOAT NULL DEFAULT NULL,
  PRIMARY KEY (`idproductos`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `dbtienda`.`usuarios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbtienda`.`usuarios` ;

CREATE TABLE IF NOT EXISTS `dbtienda`.`usuarios` (
  `idusuario` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `telefono` INT NOT NULL,
  `mail` VARCHAR(45) NOT NULL,
  `direccion` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idusuario`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `dbtienda`.`ventas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbtienda`.`ventas` ;

CREATE TABLE IF NOT EXISTS `dbtienda`.`ventas` (
  `idventas` INT NOT NULL,
  `fechaventa` DATE NOT NULL,
  `cantidad` INT NOT NULL,
  `categoria` VARCHAR(45) NOT NULL,
  `subtotal` FLOAT NOT NULL,
  `iva` FLOAT NOT NULL,
  `totalconiva` FLOAT NOT NULL,
  `idtendero` INT NULL DEFAULT NULL,
  PRIMARY KEY (`idventas`),
  INDEX `fk_tendero_idtendero_idx` (`idtendero` ASC) VISIBLE,
  CONSTRAINT `fk_tendero_idtendero`
    FOREIGN KEY (`idtendero`)
    REFERENCES `dbtienda`.`usuarios` (`idusuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
